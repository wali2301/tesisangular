import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

	/**
	* Interval to update the chart
	* @var {any} intervalUpdate
	*/
	private intervalUpdate: any = null;

	/**
	* The ChartJS Object
	* @var {any} chart
	*/
	public chart1: any = null;
	public chart2: any = null;

	/**
	* Constructor
	*/
	constructor(private http: HttpClient) {

	}

	/**
	* On component initialization
	* @function ngOnInit
	* @return {void}
	*/
	private ngOnInit(): void {
		this.chart1 = new Chart('realtime1', {
			type: 'line',
			data: {
				labels: [],
				datasets: [
				  {
					label: 'Frecuencia Cardiaca',
					fill: false,
					data: [],
					backgroundColor: '#168ede',
					borderColor: '#168ede'
				  }
				]
			  },
			  options: {
				tooltips: {
					enabled: false
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'white'
					}
				},
				scales: {
				  yAxes: [{
					  ticks: {
						  fontColor: "white"
					  }
				  }],
				  xAxes: [{
					ticks: {
						fontColor: "white",
						beginAtZero: true
					}
				  }]
				}
			  }
		});
		this.chart2 = new Chart('realtime2', {
			type: 'line',
			data: {
				labels: [],
				datasets: [
				  {
					label: 'Saturación de Oxigeno',
					fill: false,
					data: [],
					backgroundColor: '#168ede',
					borderColor: '#168ede'
				  }
				]
			  },
			  options: {
				tooltips: {
					enabled: false
				},
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						fontColor: 'white'
					}
				},
				scales: {
				  yAxes: [{
					  ticks: {
						  fontColor: "white"
					  }
				  }],
				  xAxes: [{
					ticks: {
						fontColor: "white",
						beginAtZero: true
					}
				  }]
				}
			  }
		});

		this.showData();

		this.intervalUpdate = setInterval(function(){
			this.showData();
		}.bind(this), 1000);
	}

	/**
	* On component destroy
	* @function ngOnDestroy
	* @return {void}
	*/
	private ngOnDestroy(): void {
		clearInterval(this.intervalUpdate);
	}

	/**
	* Print the data to the chart
	* @function showData
	* @return {void}
	*/
	private showData(): void {
		this.getFromAPI().subscribe(response => {
			if(response.error === false) {
				let chartTime: any = new Date();
				chartTime = chartTime.getHours() + ':' + ((chartTime.getMinutes() < 10) ? '0' + chartTime.getMinutes() : chartTime.getMinutes()) + ':' + ((chartTime.getSeconds() < 10) ? '0' + chartTime.getSeconds() : chartTime.getSeconds());
				if(this.chart1.data.labels.length > 15) {
						this.chart1.data.labels.shift();
						this.chart1.data.datasets[0].data.shift();
				}
				this.chart1.data.labels.push(chartTime);
				this.chart1.data.datasets[0].data.push(response.data1);
				this.chart1.update();
				if(this.chart2.data.labels.length > 15) {
						this.chart2.data.labels.shift();
						this.chart2.data.datasets[0].data.shift();
				}
				this.chart2.data.labels.push(chartTime);
				this.chart2.data.datasets[0].data.push(response.data2);
				this.chart2.update();
			} else {
				console.error("ERROR: The response had an error, retrying");
			}
		}, error => {
			console.error("ERROR: Unexpected response");
		});
	}

	/**
	* Get the data from the API
	* @function getFromAPI
	* @return {Observable<any>}
	*/
	private getFromAPI(): Observable<any>{
	  return this.http.get(
		'http://localhost:3000',
		{ responseType: 'json' }
	  );
	}

}
